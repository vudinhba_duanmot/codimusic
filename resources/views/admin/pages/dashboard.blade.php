@extends('layouts.admin')

@section('title', 'Dashboard')
@section('description', 'This is the dashboard')

@section('content')
    <!-- End Modal -->
    <script src="/plugins/ckeditor/ckeditor.js"></script>

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Admin <small>EDITOR CUSTOMIZE</small>
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> Ckeditor Customize
                    </li>
                </ol>
                @include('notifications.status_message')
                @include('notifications.errors_message')
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12 control-button">
                <button class="btn btn-primary btn-sm" id="check_access"><span class="glyphicon glyphicon-ok" ></span>&nbsp;Kiểm tra khoảng trắng</button>
                <hr>
            </div>
            <div class="col-lg-12 editor">

                <form>
                    {{ csrf_field() }}
                    <textarea name="editor1" id="editor1" rows="10" cols="80" >

                    </textarea>
                    <br>
                    <a class="btn btn-primary btn-sm" id="sub_dash" type="button" >Submit</a>
                </form>
            </div>
        </div>

        <!--   row  -->
        <div class="rows">
            <hr>
            <div class="label_name ">
                <p class="text-info" style="font-size:16px; font-weight: bold;">Kết quả </p>
                <span class="tool-fix">

                    <span class="btn btn-sm btn-primary"  onclick="removeWSP(this)"><span class="glyphicon glyphicon-wrench"></span>&nbsp; Sửa nhanh</span>
                    <span class="btn btn-sm btn-warning" id="other-fc"><span class="glyphicon glyphicon-wrench"  ></span>&nbsp; Other Fc</span>
                    <span class="btn btn-sm btn-danger" id="win-open"><span class="glyphicon glyphicon-wrench"  ></span>&nbsp; Window Open</span>
                </span>

            </div>
            <br>
            <div class="result" style="height: 300px; border: 1px solid steelblue">
                <div  id="show">
                </div>

            </div>

        </div>
        <div id="loginbutton">

        </div>
        <div id="feedbutton">

        </div>
        <!-- ./row -->
        <!-- Modal fix Link -->
        <div class="show_modal">

        </div>
    </div>



        <script >

            $(document).ready(function(){
                // todo: Save content to Database
                var token = "{{csrf_token()}}";
                saveData(token);

                // todo: Config of Ckeditor: in configCkeditor.js and add new plugin to modify link plugin
                CKEDITOR.replace( 'editor1', {
                    customConfig : '/js/configCkeditor.js',
                    extraPlugins : 'timestamp'
                });

                // todo: Check blank characters, if there are more two continued space character then show message warning
                accessibilityCheck(token);

            });
        </script>


@endsection