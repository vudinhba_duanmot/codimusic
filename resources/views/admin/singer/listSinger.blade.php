@extends('layouts.admin')

@section('title', 'List Singers')
@section('description', 'This is a blank page that needs to be implemented')

@section('content')
    <div class="container">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Admin <small>Ca sĩ, diễn viên</small>
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> Danh sách ca sĩ, diễn viên
                    </li>
                </ol>
                @include('notifications.status_message')
                @include('notifications.errors_message')
            </div>
        </div>
        <!-- /.row -->
        <button class="btn btn-primary">Thêm ca sĩ</button>
        <hr>
        <div class="row">
            <div class="col-lg-12">
                <table class="table">
                    <thead>
                    <td></td>
                    <td>Nghệ danh</td>
                    <td></td>
                    </thead>
                    <tbody>
                        @foreach($result as $i => $member)
                            <tr>
                                <td><img src="http://avatar.nct.nixcdn.com/singer/avatar/2016/01/25/4/1/1/7/1453716929715_600.jpg"
                                         width="100px" height="100px"></td>
                                <td>{{$member['stageName']}}</td>
                                <td><a href="/admin/singer/detail/{{$member['id']}}">Chi tiết</a></td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->
@endsection