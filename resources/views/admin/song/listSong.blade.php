@extends('layouts.admin')

@section('title', 'List Singers')
@section('description', 'This is a blank page that needs to be implemented')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Admin <small>Bài hát</small>
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> Danh sách bài hát
                    </li>
                </ol>
                @include('notifications.status_message')
                @include('notifications.errors_message')
            </div>
        </div>
        <!-- /.row -->
        <hr>
        <div class="row">
            <div class="col-lg-12">
                <table class="table">
                    <thead>
                    <td>STT</td>
                    <td>Bài hát</td>
                    <td>Người soạn</td>
                    <td>Loại nhạc</td>
                    <td>Album</td>
                    <td></td>
                    </thead>
                    <tbody>
                    @foreach($result as $i => $member)
                        <tr>
                            <td>{{$i + 1}}</td>
                            <td>{{$member['name']}}</td>
                            <td>{{$member['composer']['realName']}}</td>
                            <td>{{$member['musicType']['name']}}</td>
                            <td>{{$member['album']['name']}}</td>
                            <td><a href="/admin/song/listen/{{$member['id']}}">Nghe thử</a></td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->
@endsection