<?php

namespace ckeditor\Models;

use Illuminate\Database\Eloquent\Model;

class Singers extends Model
{
    protected $table = 'singers';

    //todo: Get music type of singer : Ca si, nhac si, dien vien, dao dien ..
    public function musicType()
    {
        return $this->belongsToMany('ckeditor\Models\MusicType', 'singer_types', 'singer_id',
            'musicType_id');
    }

    //todo: Get company of singer
    public function company()
    {
        return $this->belongsTo('ckeditor\Models\Company', 'company_id');
    }
}
