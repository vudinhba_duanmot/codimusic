<?php

namespace ckeditor\Models;

use Illuminate\Database\Eloquent\Model;

class ActivationKey extends Model
{
    /**
     * The database table is used in model
     *
     * @var string
     */
    protected $table = 'activation_keys';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     *
     * What is belongto, i will search it at other time
     */
    public function user() {
        return  $this->belongsTo(User::class);
    }
}
