<?php

namespace ckeditor\Http\Controllers\Front;

use Illuminate\Http\Request;
use ckeditor\Http\Controllers\Controller;

class ContactController extends Controller
{
    /**
     * return Contact Page
     * @params : No params
     */
    public function index()
    {
        return view('front.pages.contacts');

    }
}
