<?php

namespace ckeditor\Http\Controllers\Front;

use Illuminate\Http\Request;
use ckeditor\Http\Controllers\Controller;

class PagesController extends Controller
{
    /**
     * return Homepage
     * @params : No params
     */
    public function getHome()
    {

        
        return view('front.pages.demo');
    }
}
