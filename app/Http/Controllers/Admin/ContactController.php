<?php

namespace ckeditor\Http\Controllers\Admin;

use Illuminate\Http\Request;
use ckeditor\Http\Controllers\Controller;
use ckeditor\Models\Contacts;

class ContactController extends Controller
{
    /**
     * Get list Contacts from user
     *
     * @param Request $request
     * @return $this
     */
    public function index(Request $request)
    {
        $result = Contacts::select('name', 'email', 'address', 'content')->get();

        return view('admin.contact.listContact')->with('result', $result);
    }

    public function addContact (Request $request)
    {
//        $contact = new Contacts();
//        $contact->name = 'Cao thi Men';
//        $contact->email = 'caothimen@gmail.com';
//        $contact->address = 'Ca Mau';
//        $contact->content = 'Duoc lam cac ong!';
//        $result = $contact->save();
//        var_dump($result); die;


    }
}
