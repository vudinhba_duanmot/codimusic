<?php

namespace ckeditor\Http\Controllers\Admin;

use Illuminate\Http\Request;
use ckeditor\Http\Controllers\Controller;
use ckeditor\Models\Songs;

class SongController extends Controller
{
    public function index()
    {
        $result = Songs::with(['album', 'composer', 'musicType'])->get();

        return view('admin.song.listSong')->with('result', $result);
    }

    public function addSong()
    {
//        $song = new Songs();
//        $song->name = 'Co hang xom';
//        $song->description = 'Nhung ca khuc noi tieng cua Quang le';
//        $song->composer_id = 4;
//        $song->musicType_id = 3;
//        $song->link = 'http://www.nhaccuatui.com/bai-hat/buc-thu-tinh-dau-tien-tan-minh.FdapdwRL5bJZ.html';
//        $result = $song->save();
//        var_dump($result); die;
    }

    public function listen($song_id)
    {
        if (is_numeric($song_id)) {
            $result = Songs::with(['album', 'composer', 'musicType'])
                ->where('id', $song_id)
                ->get();

            return view('admin.song.listen')->with('result', $result);
        }

    }
}
