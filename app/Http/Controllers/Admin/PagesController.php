<?php

namespace ckeditor\Http\Controllers\Admin;

use Illuminate\Http\Request;
use ckeditor\Http\Controllers\Controller;
use ckeditor\Models\User;
use ckeditor\Models\Content;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;
use Spatie\Permission\Traits\HasRoles;


class PagesController extends Controller
{
    use HasRoles;
    protected $guard_name = 'web';
    public function getDashboard(Request $request)
    {

        return view('admin.pages.dashboard', ['text_result'=>'']);
    }

    /**
     * After dashboard return blank page
     *
     * @params : anable to write after
     */
    public function getBlank()
    {
        return view('admin.pages.blank');
    }

    /**
     * receive value from post form user and filter xss to return client
     * Display in box result of ckeditor
     *
     * @params : content of textara
     * ##return : content after filtering special characters
     */
    public function getInfo($userId)
    {

        $info = User::all();

        return response()->json($info, '200');

    }

    /**
     * Get Content from Ckeditor and save it to DB
     * Get userId from Auth and content from user to save DB
     *
     * @params content and timestamp
     */
    public function saveContent(Request $request)
    {
        if($request->method() === 'POST') {

            $data = $request->all();
            $content = $data['content'];
            $userId = Auth::user()->getAuthIdentifier();

            $info = Content::create([
                "userId" => $userId,
                "content" => $content
            ]);

            return response()->json($info);

        }
    }

    /**
     *  Check accessibility Content.
     *  If have many consecutive whtiespace, will style css this by
     *  insert a checkbox [input, lable] to notify user
     */
    public function accessibilityCheck(Request $request)
    {
        if($request->method() === 'POST') {
            $data = $request->all();
            $content = $data['content'];
            $patterRegex ='/((\s?\s*&nbsp;\s*\s?)+)|( ){2,}/';
            $count = 1;

            // todo insert a checkbox [input, lable] to notify user
            $newContent = preg_replace_callback($patterRegex, function($match) use (&$count) {
                $str = ("<input type='checkbox' name='acc_error[]' value='0' id='accerr_" . $count
                    . "' checked='' class='remv-check-" . $count . "'>"
                    . "<label for='accerr_" . $count . "' title='khongtrang' class='remv-check-"
                    . $count . "' style='background-color: #ebccd1' "
                    . "onclick='cxPopUpOpen(this)' data-url='{$match[1]}'>{$match[1]}</label>");
                $count++;
                return $str;
            }, $content);

            return response()->json(array(
                'newContent' => $newContent
            ));

        }
    }

}
