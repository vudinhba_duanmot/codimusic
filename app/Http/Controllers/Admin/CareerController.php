<?php

namespace ckeditor\Http\Controllers\Admin;

use Illuminate\Http\Request;
use ckeditor\Http\Controllers\Controller;
use ckeditor\Models\Careers;

class CareerController extends Controller
{
    /**
     * Get list Careers : singer, composer, actor ...
     *
     * @param Request $request
     * @return $this
     */
    public function index(Request $request)
    {
        $result = Careers::select('career')->get();

        return view('admin.career.listCareer')->with('result', $result);
    }

    public function addContact (Request $request)
    {
//        $career = new Careers();
//        $career->career = 'Nhạc công';
//        $result = $career->save();
//        var_dump($result); die;


    }
}
