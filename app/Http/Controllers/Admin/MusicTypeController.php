<?php

namespace ckeditor\Http\Controllers\Admin;

use Illuminate\Http\Request;
use ckeditor\Http\Controllers\Controller;
use ckeditor\Models\MusicType;

class MusicTypeController extends Controller
{
    /**
     * Get list Careers : singer, composer, actor ...
     *
     * @param Request $request
     * @return $this
     */
    public function index(Request $request)
    {
        $result = MusicType::select('name')->get();

        return view('admin.musicType.listType')->with('result', $result);
    }

    public function addType (Request $request)
    {
        $type = new MusicType();
        $type->name = 'Miền trung';
        $result = $type->save();
        var_dump($result); die;


    }
}
