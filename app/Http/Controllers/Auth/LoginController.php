<?php

namespace ckeditor\Http\Controllers\Auth;

use ckeditor\Http\Controllers\Controller;

use Illuminate\Support\Facades\Log;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Auth guard
     *
     * @var
     */
    protected $auth;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(Guard $auth)
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        Log:info("Client loggin system : " . $ip);
        $this->middleware('guest', ['except' => 'logout']);
        $this->auth = $auth;

    }

    public function login(Request $request)
    {
        $username      = $request->get('username');
        $password   = $request->get('password');
        $remember   = $request->get('remember');

        if ($this->auth->attempt([
            'username'     => $username,
            'password'  => $password,
            'activated'  => 1,
        ], $remember == 1 ? true : false)) {

            return redirect()->route('admin.dashboard');

        }
        else {
            return redirect()->back()
                ->with('message','Incorrect username or password')
                ->with('status', 'danger')
                ->withInput();
        }

    }
}
