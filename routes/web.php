<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',         ['as' => 'front.home',   'uses' => 'Front\PagesController@getHome']);
Route::get('/audios',         ['as' => 'front.audio',   'uses' => 'Front\AudiosController@index']);
Route::get('/galleries',         ['as' => 'front.galleries',   'uses' => 'Front\GalleriesController@index']);
Route::get('/videos',         ['as' => 'front.videos',   'uses' => 'Front\VideosController@index']);
Route::get('/tourdate',         ['as' => 'front.tourdate',   'uses' => 'Front\TourDateController@index']);
Route::get('/contact',         ['as' => 'front.contact',   'uses' => 'Front\ContactController@index']);



Route::group(['namespace' => 'Admin', 'prefix' =>'admin', 'middleware' => 'auth'], function(){
    Route::get('/', ['as' => 'admin.dashboard', 'uses' => 'PagesController@getDashboard']);
    Route::post('/', ['as' => 'admin.post.dashboard', 'uses' => 'PagesController@getDashboard']);

    Route::get('/listMember', ['as' => 'admin.listMember', 'uses' => 'MemberController@index']);
    Route::get('/listSinger', ['as' => 'admin.listSinger', 'uses' => 'SingerController@index']);
    Route::get('/addSinger', ['as' => 'admin.addSinger', 'uses' => 'SingerController@addSinger']);
    Route::get('/singer/detail/{singer_id}', ['as' => 'admin.singerDetail', 'uses' => 'SingerController@singerDetail']);


    Route::get('/listAlbum', ['as' => 'admin.listAlbum', 'uses' => 'AlbumController@index']);
    Route::get('/addAlbum', ['as' => 'admin.addAlbum', 'uses' => 'AlbumController@addAlbum']);

    Route::get('/listVideo', ['as' => 'admin.listVideo', 'uses' => 'VideoController@index']);
    Route::get('/addVideo', ['as' => 'admin.addVideo', 'uses' => 'VideoController@addVideo']);

    Route::get('/listPicture', ['as' => 'admin.listPicture', 'uses' => 'PictureController@index']);
    Route::get('/addPicture', ['as' => 'admin.addPicture', 'uses' => 'PictureController@addPicture']);

    Route::get('/listContact', ['as' => 'admin.listContact', 'uses' => 'ContactController@index']);
    Route::get('/addContact', ['as' => 'admin.addContact', 'uses' => 'ContactController@addContact']);

    Route::get('/listCareer', ['as' => 'admin.listCareer', 'uses' => 'CareerController@index']);
    Route::get('/addCareer', ['as' => 'admin.addCreer', 'uses' => 'CareerController@addContact']);

    Route::get('/listMusicType', ['as' => 'admin.listMusicType', 'uses' => 'MusicTypeController@index']);
    Route::get('/addMusicType', ['as' => 'admin.addMusicType', 'uses' => 'MusicTypeController@addType']);

    Route::get('/listCompany', ['as' => 'admin.listCompany', 'uses' => 'CompanyController@index']);
    Route::get('/addCompany', ['as' => 'admin.addCompany', 'uses' => 'CompanyController@addCompany']);



    Route::get('/listSong', ['as' => 'admin.listSong', 'uses' => 'SongController@index']);
    Route::get('/addSong', ['as' => 'admin.addSong', 'uses' => 'SongController@addSong']);
    Route::get('/song/listen/{song_id}', ['as' => 'admin.listen', 'uses' => 'SongController@listen']);

});
// auth routes setup
Auth::routes();



// registration activation routes
Route::get('activation/key/{activation_key}', ['as' => 'activation_key', 'uses' => 'Auth\ActivationKeyController@activateKey']);
Route::get('activation/resend', ['as' =>  'activation_key_resend', 'uses' => 'Auth\ActivationKeyController@showKeyResendForm']);
Route::post('activation/resend', ['as' =>  'activation_key_resend.post', 'uses' => 'Auth\ActivationKeyController@resendKey']);


// username forgot_username
Route::get('username/reminder', ['as' =>  'username_reminder', 'uses' => 'Auth\ForgotUsernameController@showForgotUsernameForm']);
Route::post('username/reminder', ['as' =>  'username_reminder.post', 'uses' => 'Auth\ForgotUsernameController@sendUserameReminder']);