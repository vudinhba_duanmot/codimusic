/**
 * Copyright (c) 2014-2016, CKSource - Frederico Knabben. All rights reserved.
 * Licensed under the terms of the MIT License (see LICENSE.md).
 *
 * Basic sample plugin inserting current date and time into the CKEditor editing area.
 *
 * Created out of the CKEditor Plugin SDK:
 * http://docs.ckeditor.com/#!/guide/plugin_sdk_intro
 */

// Register the plugin within the editor.
CKEDITOR.plugins.add( 'timestamp', {

	// Register the icons. They must match command names.
	icons: 'timestamp',

	// The plugin initialization logic goes inside this method.
	init: function( editor ) {

        /**
         * Catch event click "new link" icon to add link.
         * After, show popup modal for User enter Url Link
         * Find Text is selected and it's position
         * And Check Url, if url is valid then add this link to selected text
         */
		editor.addCommand( 'insertTimestamp', {
			// Define the function that will be fired when the command is executed.
			exec: function( editor ) {

			    var modal_link = '';
			    modal_link = modal_link + '<div class="modal fade" tabindex="-1" role="dialog" id="modalFixLink">' +
                            '<div class="modal-dialog" role="document">' +
                            '<div class="modal-content">' +
                            '<div class="modal-header">' +
                            '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                        '<h4 class="modal-title text-info">Thêm liên kết</h4>' +
                        '</div>' +
                        '<div class="modal-body">'+
                            '<label class="text-info">Thêm link :</label>'+
                        '<input type="text" class="input-group form-control" placeholder="Nhập liên kết tại đây ..." id="link_emb" required >'+
                        '<span class="text-danger" id="err_msg"></span>'+
                            '<br>'+
                            '<br>'+
                            '<div style="font-style: italic">'+
                            '<span class="text-uppercase" style="text-decoration: underline;" >Chú ý : </span>'+
                        '<span> Chỉ thêm được liên kết vào đoạn kí tự đã được bôi đen. </span>'+
                        '<span> Chương trình vẫn đang được cải tiến, có gì sai sót vui lòng phản hồi lại địa chỉ sau : <a>0977803743</a></span>'+
                        '</div>'+
                        '<br>'+
                        '</div>'+
                        '<div class="modal-footer">'+
                            '<button type="button" class="btn btn-default" id="btn_close">Hủy bỏ</button>'+
                        '<button type="button" class="btn btn-primary" id="btn_save" >Lưu lại</button>'+
                        '</div>'+
                        '</div><!-- /.modal-content -->'+
                        '</div><!-- /.modal-dialog -->'+
                        '</div><!-- /.modal -->';
                $(".show_modal").html(modal_link);
				// todo: when close modal to reload page
                $('#btn_close').click(function() {

                    $('#modalFixLink').modal('hide');

                });



				// todo Check selected text to insert link
				var selectedContent = '';
                var selection = CKEDITOR.instances['editor1'].getSelection();

                if (selection.getType() == CKEDITOR.SELECTION_ELEMENT) {

                    selectedContent = selection.getSelectedElement().$.outerHTML;

                } else if (selection.getType() == CKEDITOR.SELECTION_TEXT) {

                    if (CKEDITOR.env.ie) {

                        selection.unlock(true);
                        selectedContent = selection.getNative().createRange().text;

                    } else {

                        selectedContent = selection.getNative();

                    }
                }
                console.log(selectedContent);

                $('#modalFixLink').modal('show');

                if(selectedContent == '') {

					$('.modal-body').html('Chú ý: Yêu cầu không được thực hiện, vui lòng bôi đen đoạn văn bản cần chèn!');

				} else {

					// todo: Embed link into selected text
                    $('#btn_save').click(function() {

                        var url = $('#link_emb').val();

                        // Todo Check Url is valid
                        var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
                            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|'+ // domain name
                            '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
                            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
                            '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
                            '(\\#[-a-z\\d_]*)?$','i'); // fragment locator

                        if(!pattern.test(url)) {

                            $('#err_msg').html('Liên kết nhập vào không hợp lệ!');

                        } else {

                            $('#err_msg').html('');

                            CKEDITOR.instances.editor1.insertHtml( '<a href="'+url+'">'+selectedContent+'</a>' );

                            $('#modalFixLink').modal('hide');

                        }
					});
				}
			}
		});

		// Create the toolbar button that executes the above command.
		editor.ui.addButton( 'Timestamp', {
			label: 'Fix Link',
			command: 'insertTimestamp',
			toolbar: 'link'
		});
	}
});
